function validate()
{

    if( document.contest.fname.value == "" )
    {
        alert( "Please provide your first name!" );
        document.contest.fname.focus() ;
        return false;
    }

    if( document.contest.lname.value == "" )
    {
        alert( "Please provide your last name!" );
        document.contest.lname.focus() ;
        return false;
    }

    if( document.contest.jobtitle.value == "" )
    {
        alert( "Please provide your job title!" );
        document.contest.jobtitle.focus() ;
        return false;
    }

    if( document.contest.email.value == "" )
    {
        var emailID = document.contest.email.value;
        atpos = emailID.indexOf("@");
        dotpos = emailID.lastIndexOf(".");

        if (atpos < 1 || ( dotpos - atpos < 2 ))
        {
            alert("Please enter a correct email address")
            document.contest.email.focus();
            return false;
        }
        return ( true );
    }

    if( document.contest.company.value == "" )
    {
        alert( "Please provide company name!" );
        document.contest.company.focus() ;
        return false;
    }

    if( document.contest.city.value == "" )
    {
        alert( "Please provide your city!" );
        document.contest.city.focus() ;
        return false;
    }

    if( document.contest.profession.value == "" )
    {
        alert( "Please provide your profession!" );
        document.contest.profession.focus() ;
        return false;
    }

    if( document.contest.state.value == "" )
    {
        alert( "Please provide your state!" );
        document.contest.state.focus() ;
        return false;
    }

    if( document.contest.tele.value == "" )
    {
        alert( "Please provide your telephone!" );
        document.contest.tele.focus() ;
        return false;
    }

    return( true );
}